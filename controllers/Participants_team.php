<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participants_team extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	public function index($id)
	{
		$this->load->model('participants_team_model', '', TRUE);
		list($participants, $team) = $this->participants_team_model->get_participants_for_team($id);
		$data['participants'] = $participants;
		$data['team'] = $team;
		$this->load->view('layout/header');
		$this->load->view('participants_team', $data);
	}

	public function create()
	{
		$this->load->model('participants_team_model', '', TRUE);
		if (isset($_POST['team_id']) && !empty($_POST['team_id'])) {
			$this->participants_team_model->update_entry();
		} else {
			$this->participants_team_model->insert_entry();
		}
	}

	public function delete()
	{
		$this->load->model('participants_model', '', TRUE);
		$this->participants_model->delete();
	}
}
