<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Match extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('match_model', '', TRUE);
		list($matchs, $tournament, $teams) = $this->match_model->get_entries();
		$data['matchs'] = $matchs;
		$data['tournament'] = $tournament;
		$data['teams'] = $teams;
		$this->load->view('layout/header');
		$this->load->view('match', $data);
	}

	public function create()
	{
		$this->load->model('match_model', '', TRUE);
		$_POST['start_date'] = strtotime($_POST['start_date']);
		$_POST['end_date'] = strtotime($_POST['end_date']);
		if (isset($_POST['id']) && !empty($_POST['id'])) {
			$this->match_model->update_entry();
		} else {
			$this->match_model->insert_entry();
		}
	}

	public function delete()
	{
		$this->load->model('match_model', '', TRUE);
		$this->match_model->delete();
	}
}
