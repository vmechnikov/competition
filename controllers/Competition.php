<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Competition extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('competition');
	}
}
