<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('team_model', '', TRUE);
		$data['teams'] = $this->team_model->get_last_ten_entries();
		$this->load->view('layout/header');
		$this->load->view('teams', $data);
	}

	public function create()
	{
		$this->load->model('team_model', '', TRUE);
		if (isset($_POST['id']) && !empty($_POST['id'])) {
			$this->team_model->update_entry();
		} else {
			$this->team_model->insert_entry();
		}
	}

	public function delete()
	{
		$this->load->model('team_model', '', TRUE);
		$this->team_model->delete();
	}
}
