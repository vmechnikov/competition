<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participants extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('participants_model', '', TRUE);
		$data['participants'] = $this->participants_model->get_entries();
		$this->load->view('layout/header');
		$this->load->view('participants', $data);
	}

	public function create()
	{
		$this->load->model('participants_model', '', TRUE);
		if (isset($_POST['id']) && !empty($_POST['id'])) {
			$this->participants_model->update_entry();
		} else {
			$this->participants_model->insert_entry();
		}
	}

	public function delete()
	{
		$this->load->model('participants_model', '', TRUE);
		$this->participants_model->delete();
	}
}
