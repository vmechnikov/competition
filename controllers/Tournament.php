<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tournament extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('tournament_model', '', TRUE);
		$data['tournament'] = $this->tournament_model->get_entries();
		$this->load->view('layout/header');
		$this->load->view('tournament', $data);
	}

	public function create()
	{
		$this->load->model('tournament_model', '', TRUE);
		$_POST['start_date'] = strtotime($_POST['start_date']);
		$_POST['end_date'] = strtotime($_POST['end_date']);
		if (isset($_POST['id']) && !empty($_POST['id'])) {
			$this->tournament_model->update_entry();
		} else {
			$this->tournament_model->insert_entry();
		}
	}

	public function delete()
	{
		$this->load->model('tournament_model', '', TRUE);
		$this->tournament_model->delete();
	}
}
