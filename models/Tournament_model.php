<?php

class Tournament_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public $winner_team_id;
	public $name;
	public $tournament_type;
	public $status;
	public $start_date;
	public $end_date;
	public $created_at;
	public $updated_at;

	public function get_entries()
	{
		$query = $this->db->get('tournament');
		return $query->result();
	}

	public function insert_entry()
	{
		$this->winner_team_id = $_POST['winner'];
		$this->name = $_POST['name'];
		$this->tournament_type = $_POST['tournament_type'];
		$this->status = $_POST['status'];
		$this->start_date = $_POST['start_date'];
		$this->end_date = $_POST['end_date'];
		$this->created_at = time();
		$this->updated_at = time();
		$this->db->insert('tournament', $this);
	}

	public function update_entry()
	{
		$this->db->where('id', $_POST['id']);
		$query = $this->db->get('tournament');
		$result = $query->result();

		$this->winner_team_id = $_POST['winner'];
		$this->name = $_POST['name'];
		$this->tournament_type = $_POST['tournament_type'];
		$this->status = $_POST['status'];
		$this->start_date = $_POST['start_date'];
		$this->end_date = $_POST['end_date'];
		$this->updated_at = time();
		$this->created_at = $result[0]->created_at;
		$this->db->update('tournament', $this, array('id' => $_POST['id']));
	}

	public function delete()
	{
		$this->db->where('id', $_POST['id']);
		$this->db->delete('tournament');
	}

}