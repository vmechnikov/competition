<?php

class Participants_team_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public $team_id;
	public $participant_id;

	public function get_participants_for_team($id)
	{
		$this->db->where('id', $id);
		$teams = $this->db->get('team');
		$this->db->select('participants.*, team_participants.team_id, team_participants.participant_id');
		$this->db->from('participants');
		$this->db->join('team_participants', "team_participants.team_id = $id AND team_participants.participant_id = participants.id", 'left');
		//$this->db->where('team_participants.participant_id', 'participants.id');
		//$this->db->join('team_participants', 'team_participants.participant_id = participants.id');
		$participants = $this->db->get();
		return [$participants->result(), $teams->result()];
	}

	public function update_entry()
	{
		$this->db->delete('team_participants', array('team_id' => $_POST['team_id']));
		foreach ($_POST['participants'] as $value){
			$this->participant_id = $value;
			$this->team_id = $_POST['team_id'];
			$this->db->insert('team_participants', $this);
		}
	}
}