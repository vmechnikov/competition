<?php

class Participants_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public $first_name;
	public $last_name;
	public $email;
	public $login;
	public $rating;
	public $status;
	public $created_at;
	public $updated_at;

	public function get_entries()
	{
		$query = $this->db->get('participants');
		return $query->result();
	}

	public function insert_entry()
	{
		$this->first_name = $_POST['first_name'];
		$this->last_name = $_POST['last_name'];
		$this->email = $_POST['email'];
		$this->login = $_POST['login'];
		$this->rating = $_POST['rating'];
		$this->status = $_POST['status'];
		$this->created_at = time();
		$this->updated_at = time();
		$this->db->insert('participants', $this);
	}

	public function update_entry()
	{
		$this->db->where('id', $_POST['id']);
		$query = $this->db->get('participants');
		$result = $query->result();

		$this->first_name = $_POST['first_name'];
		$this->last_name = $_POST['last_name'];
		$this->email = $_POST['email'];
		$this->login = $_POST['login'];
		$this->rating = $_POST['rating'];
		$this->status = $_POST['status'];
		$this->updated_at = time();
		$this->created_at = $result[0]->created_at;
		$this->db->update('participants', $this, array('id' => $_POST['id']));
	}

	public function delete()
	{
		$this->db->where('id', $_POST['id']);
		$this->db->delete('participants');
	}

}