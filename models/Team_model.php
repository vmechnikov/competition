<?php

class Team_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public $name;
	public $status;
	public $created_at;
	public $updated_at;

	public function get_last_ten_entries()
	{
		$query = $this->db->get('team', 10);
		return $query->result();
	}

	public function insert_entry()
	{
		$this->name = $_POST['name'];
		$this->status = $_POST['status'];
		$this->created_at = time();
		$this->updated_at = time();
		$this->db->insert('team', $this);
	}

	public function update_entry()
	{
		$this->db->where('id', $_POST['id']);
		$query = $this->db->get('team');
		$result = $query->result();

		$this->name = $_POST['name'];
		$this->status = $_POST['status'];
		$this->updated_at = time();
		$this->created_at = $result[0]->created_at;

		$this->db->update('team', $this, array('id' => $_POST['id']));
	}

	public function delete()
	{
		$this->db->where('id', $_POST['id']);
		$this->db->delete('team');
	}

}