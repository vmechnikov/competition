<?php

class Match_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public $match_type;
	public $name;
	public $status;
	public $tournament_id;
	public $first_team_id;
	public $second_team_id;
	public $score_match;
	public $winner_team_id;
	public $start_date;
	public $end_date;
	public $created_at;
	public $updated_at;

	public function get_entries()
	{
		$matchs = $this->db->get('matchs');
		$this->db->where('status', 'active');
		$tournaments = $this->db->get('tournament');
		$this->db->where('status', 'active');
		$teams = $this->db->get('team');
		return [$matchs->result(), $tournaments->result(), $teams->result()];
	}

	public function insert_entry()
	{

		if(empty($_POST['tournament_id'])){
			$_POST['tournament_id'] = null;
		}
		if(empty($_POST['first_team_id'])){
			$_POST['first_team_id'] = null;
		}
		if(empty($_POST['second_team_id'])){
			$_POST['second_team_id'] = null;
		}
		if(empty($_POST['winner_team_id'])){
			$_POST['winner_team_id'] = null;
		}

		$this->match_type = $_POST['match_type'];
		$this->name = $_POST['name'];
		$this->status = $_POST['status'];
		$this->tournament_id = $_POST['tournament_id'];
		$this->first_team_id = $_POST['first_team_id'];
		$this->second_team_id = $_POST['second_team_id'];
		$this->score_match = $_POST['score_match'];
		$this->winner_team_id = $_POST['winner_team_id'];
		$this->start_date = $_POST['start_date'];
		$this->end_date = $_POST['end_date'];
		$this->created_at = time();
		$this->updated_at = time();
		$this->db->insert('matchs', $this);
	}

	public function update_entry()
	{
		$this->db->where('id', $_POST['id']);
		$query = $this->db->get('match');
		$result = $query->result();

		$this->match_type = $_POST['match_type'];
		$this->name = $_POST['name'];
		$this->status = $_POST['status'];
		$this->tournament_id = $_POST['tournament_id'];
		$this->first_team_id = $_POST['first_team_id'];
		$this->second_team_id = $_POST['second_team_id'];
		$this->score_match = $_POST['score_match'];
		$this->winner_team_id = $_POST['winner_team_id'];
		$this->start_date = $_POST['start_date'];
		$this->end_date = $_POST['end_date'];
		$this->updated_at = time();
		$this->created_at = $result[0]->created_at;
		$this->db->update('matchs', $this, array('id' => $_POST['id']));
	}

	public function delete()
	{
		$this->db->where('id', $_POST['id']);
		$this->db->delete('matchs');
	}

}