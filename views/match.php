<div id="container">
    <h1>Match</h1>
    <div id="body">

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
            Add button
        </button>
        <br>
        <br>
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Match type</th>
                <th>Name</th>
                <th>Status</th>
                <th>Tournament</th>
                <th>First Team</th>
                <th>Second Team</th>
                <th>Score Match</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Winner</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Match type</th>
                <th>Name</th>
                <th>Status</th>
                <th>Tournament</th>
                <th>First Team</th>
                <th>Second Team</th>
                <th>Score Match</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Winner</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
            </tr>
            </tfoot>
            <tbody>
			<?php foreach ($matchs as $t) { ?>
                <tr>
                    <td><?php echo $t->match_type; ?></td>
                    <td><?php echo $t->name; ?></td>
                    <td><?php echo $t->status; ?></td>
                    <td>
						<?php
						foreach ($tournament as $tu) {
							if ($tu->id == $t->first_team_id) {
								echo $tu->name;
								break;
							}
						}
						?>
                    </td>
                    <td>
						<?php
						foreach ($teams as $tu) {
							if ($tu->id == $t->first_team_id) {
								echo $tu->name;
								break;
							}
						}
						?>
                    </td>
                    <td>
						<?php
						foreach ($teams as $tu) {
							if ($tu->id == $t->second_team_id) {
								echo $tu->name;
								break;
							}
						}
						?>
                    </td>
                    <td><?php echo $t->score_match; ?></td>
                    <td><?php echo date("m/d/Y h:i A", $t->start_date) ?></td>
                    <td><?php echo date("m/d/Y h:i A", $t->end_date) ?></td>
                    <td><?php echo $t->winner_team_id; ?></td>
                    <td><?php echo date('d F Y h:i:s A', $t->created_at); ?></td>
                    <td><?php echo date('d F Y h:i:s A', $t->updated_at); ?></td>
                    <td>
                        <span class="glyphicon glyphicon-remove remove_item" style="cursor: pointer;"
                              data-id="<?php echo $t->id; ?>"></span>
                        <span class="glyphicon glyphicon-edit edit_item" style="cursor: pointer;"
                              data-match_type="<?php echo $t->match_type; ?>"
                              data-name="<?php echo $t->name; ?>"
                              data-status="<?php echo $t->status; ?>"
                              data-tournament_id="<?php echo $t->tournament_id; ?>"
                              data-first_team_id="<?php echo $t->first_team_id; ?>"
                              data-second_team_id="<?php echo $t->second_team_id; ?>"
                              data-score_match="<?php echo $t->score_match; ?>"
                              data-start_date="<?php echo date("m/d/Y h:i A", $t->start_date) ?>"
                              data-end_date="<?php echo date("m/d/Y h:i A", $t->end_date) ?>"
                              data-winner_team_id="<?php echo $t->winner_team_id; ?>
							  data-id="<?php echo $t->id; ?>"
                        >
                        </span>
                    </td>
                </tr>
			<?php } ?>
            </tbody>
        </table>
    </div>

    <p class="footer">Page rendered in <strong>{elapsed_time}</strong>
        seconds. <?php echo (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?>
    </p>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Match</h4>
            </div>
            <div class="modal-body">
                <form id="add_form" method="post" action="/match/create">
                    <input type="hidden" name="id">

                    <div class="form-group">
                        <label>Match Type</label>
                        <select id="match_type" class="form-control" name="match_type">
                            <option value="one">one to one</option>
                            <option value="team">team</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="name" name="name">
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <select id="status" class="form-control" name="status">
                            <option value="active">Active</option>
                            <option value="deactivate">Deactivate</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Tournament</label>
                        <select id="tournament_id" class="form-control" name="tournament_id">
							<?php foreach ($tournament as $t) { ?>
                                <option value="<?php echo $t->id; ?>"><?php echo $t->name; ?></option>
							<?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>First Team</label>
                        <select id="first_team_id" class="form-control" name="first_team_id">
                            <option value="">...</option>
							<?php foreach ($teams as $m) { ?>
                                <option value="<?php echo $m->id; ?>"><?php echo $m->name; ?></option>
							<?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Second Team Id</label>
                        <select id="second_team_id" class="form-control" name="second_team_id">
                            <option value="">...</option>
							<?php foreach ($teams as $m) { ?>
                                <option value="<?php echo $m->id; ?>"><?php echo $m->name; ?></option>
							<?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="text" class="form-control" id="start_date" placeholder="start_date"
                               name="start_date">
                    </div>

                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="text" class="form-control" id="end_date" placeholder="end_date" name="end_date">
                    </div>

                    <div class="form-group">
                        <label for="score_match">Score Match</label>
                        <input type="text" class="form-control" id="score_match" placeholder="score_match"
                               name="score_match">
                    </div>

                    <div class="form-group">
                        <label>Winner Team</label>
                        <select id="winner_team_id" class="form-control" name="winner_team_id">
                            <option value="">...</option>
							<?php foreach ($teams as $m) { ?>
                                <option value="<?php echo $m->id; ?>"><?php echo $m->name; ?></option>
							<?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="submit_button" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


<!--remove confirm modal-->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            Are you sure want remove this match?
        </div>
    </div>
</div>

<div class="modal fade" id="deleteConfirmModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p> Are you sure want remove this match?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="delete_btn" type="button" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>

<script>


    $('#start_date').datetimepicker().on("dp.show", function () {
        console.log("dtp open");
    });

    $('#end_date').datetimepicker();


    /*init datatable*/
    $(document).ready(function () {
        $('#example').DataTable();
    });

    /*onclick button submit form*/
    $("#submit_button").on("click", function () {
        $("#add_form").submit();
    });

    /*on submit form*/
    $("#add_form").submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "/match/create",
            method: "post",
            data: $('#add_form').serialize(),
            success: function (result) {
                location.reload();
            },
            error: function (result) {
                alert("Some error");
            }
        });
    });

    /*onclick edit tournament insert data and open modal*/
    $(".edit_item").on("click", function () {

        var id, match_type, tournament_id, first_team_id, second_team_id, score_match, name, start_date, end_date,
            winner_team_id, status;

        name = $(this).attr("data-name");
        first_team_id = $(this).attr("data-first_team_id");
        second_team_id = $(this).attr("data-second_team_id");
        match_type = $(this).attr("data-match_type");
        tournament_id = $(this).attr("data-tournament_id");
        start_date = $(this).attr("data-start_date");
        end_date = $(this).attr("data-end_date");
        winner_team_id = $(this).attr("data-winner_team_id");
        score_match = $(this).attr("data-score_match");
        status = $(this).attr("data-status");
        id = $(this).attr("data-id");

        $('#start_date').data("DateTimePicker").date(start_date);
        $("input[name='name']").val(name);
        $("input[name='start_date']").val(start_date);
        $("input[name='end_date']").val(end_date);
        $("input[name='winner_team_id']").val(winner_team_id);
        $("input[name='score_match']").val(score_match);
        $("input[name='id']").val(id);

        $("#status option[value=" + status + "]").attr('selected', 'selected');
        $("#tournament_id option[value=" + tournament_id + "]").attr('selected', 'selected');
        $("#match_type option[value=" + match_type + "]").attr('selected', 'selected');

        if (typeof first_team_id != "undefined") {
            $("#first_team_id option[value=" + first_team_id + "]").attr('selected', 'selected');
        }

        if (typeof second_team_id != "undefined") {
            $("#second_team_id option[value=" + second_team_id + "]").attr('selected', 'selected');
        }

        if (typeof winner_team_id != "undefined") {
            $("#winner_team_id option[value=" + winner_team_id + "]").attr('selected', 'selected');
        }

        $("#match_type option[value=" + match_type + "]").attr('selected', 'selected');
        $('#myModal').modal('show');
    });


    /*on hide modal event*/
    $('#myModal').on('hide.bs.modal', function () {
        $("input[name='name']").val('');
        $("input[name='id']").val('');
        $("#status").removeAttr('selected');
    });

    $(".remove_item").on("click", function () {
        var id;
        id = $(this).attr('data-id');
        $("#delete_btn").attr("data-id", id);
        $("#deleteConfirmModal").modal('show');
    });

    $("#delete_btn").on("click", function () {
        var id;
        id = $(this).attr('data-id');
        $.ajax({
            url: "/match/delete",
            method: "post",
            data: {id: id},
            success: function (result) {
                location.reload();
            },
            error: function (result) {
                alert("Some error");
            }
        });
    });
</script>

</body>
</html>