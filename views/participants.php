<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>


<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
		  integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
		  integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
			integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
			crossorigin="anonymous"></script>

	<!--dataTables-->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

	<style type="text/css">

		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>

</head>
<body>

<div id="container">
	<h1>Participants</h1>
	<div id="body">

		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
			Add button
		</button>
		<br>
		<br>
		<table id="example" class="display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>email</th>
				<th>login</th>
				<th>rating</th>
				<th>status</th>
				<th>Created At</th>
				<th>Updated At</th>
				<th>Actions</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>email</th>
				<th>login</th>
				<th>rating</th>
				<th>status</th>
				<th>Created At</th>
				<th>Updated At</th>
				<th>Actions</th>
			</tr>
			</tfoot>
			<tbody>
			<?php foreach ($participants as $t){ ?>
				<tr>
					<td><?php echo $t->first_name;?></td>
					<td><?php echo $t->last_name;?></td>
					<td><?php echo $t->email;?></td>
					<td><?php echo $t->login;?></td>
					<td><?php echo $t->rating;?></td>
					<td><?php echo $t->status;?></td>
					<td><?php echo date('d F Y h:i:s A', $t->created_at);?></td>
					<td><?php echo date('d F Y h:i:s A', $t->updated_at);?></td>
					<td>
						<span class="glyphicon glyphicon-remove remove_item" style="cursor: pointer;" data-id="<?php echo $t->id;?>"></span>
						<span class="glyphicon glyphicon-edit edit_item" style="cursor: pointer;"
							  data-first_name="<?php echo $t->first_name;?>"
							  data-last_name="<?php echo $t->last_name;?>"
							  data-email="<?php echo $t->email;?>"
							  data-login="<?php echo $t->login;?>"
							  data-rating="<?php echo $t->rating;?>"
							  data-status="<?php echo $t->status;?>"
							  data-id="<?php echo $t->id;?>"
						>
                    </span>
					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong>
		seconds. <?php echo (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?>
	</p>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Team</h4>
			</div>
			<div class="modal-body">
				<form id="add_form" method="post" action="/participants/create">
					<input type="hidden" name="id">
					<div class="form-group">
						<label for="name">First Name</label>
						<input type="text" class="form-control" id="name" placeholder="first_name" name="first_name">
					</div>
					<div class="form-group">
						<label for="name">Last Name</label>
						<input type="text" class="form-control" id="name" placeholder="last_name" name="last_name">
					</div>
					<div class="form-group">
						<label for="name">Email</label>
						<input type="email" class="form-control" id="name" placeholder="email" name="email">
					</div>
                    <div class="form-group">
                        <label for="name">Login</label>
                        <input type="text" class="form-control" id="login" placeholder="login" name="login">
                    </div>
					<div class="form-group">
						<label for="name">Rating</label>
						<input type="text" class="form-control" id="name" placeholder="rating" name="rating">
					</div>

					<div class="form-group">
						<label>Status</label>
						<select id="status" class="form-control" name="status">
							<option value="active">Active</option>
							<option value="deactivate">Deactivate</option>
						</select>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="submit_button" type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>


<!--remove confirm modal-->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			Are you sure want remove this participants?
		</div>
	</div>
</div>

<div class="modal fade" id="deleteConfirmModal" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">
				<p> Are you sure want remove this participants?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="delete_btn" type="button" class="btn btn-danger">Delete</button>
			</div>
		</div>
	</div>
</div>

<script>
    /*init datatable*/
    $(document).ready(function () {
        $('#example').DataTable();
    });

    /*onclick button submit form*/
    $("#submit_button").on("click", function () {
        $( "#add_form" ).submit();
    });

    /*on submit form*/
    $( "#add_form" ).submit(function( event ) {
        event.preventDefault();
        $.ajax({
            url: "/participants/create",
            method: "post",
            data: $('#add_form').serialize(),
            success: function (result) {
                location.reload();
            },
            error: function (result) {
                alert("Some error");
            }
        });
    });

    /*onclick edit participants insert data and open modal*/
    $(".edit_item").on("click", function () {
        var first_name, last_name, email, login, rating, status, id;
        first_name = $(this).attr("data-first_name");
        last_name = $(this).attr("data-last_name");
        email = $(this).attr("data-email");
        rating = $(this).attr("data-rating");
        status = $(this).attr("data-status");
        login = $(this).attr("data-login");
        id = $(this).attr("data-id");
        $("input[name='first_name']").val(first_name);
        $("input[name='last_name']").val(last_name);
        $("input[name='email']").val(email);
        $("input[name='login']").val(login);
        $("input[name='rating']").val(rating);
        $("input[name='status']").val(status);
        $("input[name='id']").val(id);
        $("#status option[value="+status+"]").attr('selected','selected');
        $('#myModal').modal('show');
    });

    /*on hide modal event*/
    $('#myModal').on('hide.bs.modal', function () {
        $("input[name='name']").val('');
        $("input[name='id']").val('');
        $("#status").removeAttr('selected');
    });

    $(".remove_item").on("click", function () {
        var id;
        id = $(this).attr('data-id');
        $("#delete_btn").attr("data-id", id);
        $("#deleteConfirmModal").modal('show');
    });

    $("#delete_btn").on("click", function () {
        var id;
        id = $(this).attr('data-id');
        $.ajax({
            url: "/participants/delete",
            method: "post",
            data: {id: id},
            success: function (result) {
                location.reload();
            },
            error: function (result) {
                alert("Some error");
            }
        });
    });
</script>

</body>
</html>